#pragma once
namespace Textures
{
    enum class ID
    {
        Water,
        PlayerType1,
        PlayerType2,
        EnemyType1,
        EnemyType2,
        EnemyType3
    };
}