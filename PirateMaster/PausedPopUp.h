#pragma once
#include "SceneNode.h"
#include <SFML/Graphics.hpp> 
#include <string>
#include <sstream>

class PausedPopUp : public SceneNode
{
public:
	explicit PausedPopUp(const std::string& string, const sf::Font& font, const int charSize);
private:
	virtual void drawCurrent(sf::RenderTarget& target, sf::RenderStates states) const;
private:
	sf::Text mText;
};