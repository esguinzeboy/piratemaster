#pragma once
#include "Entity.h"
#include "TexturesIdEnum.h"
#include "AnimationControlEnum.h"
#include "ResourceHolder.h"
#include <SFML/Graphics.hpp>

class Player : public Entity 
{

public:
	enum Type
	{
		PlayerType1,
		PlayerType2,
	};
public:
	explicit Player(Type type, ResourceHolder<sf::Texture, Textures::ID>& textureHolder,float increasePlayerSpeed);
	virtual unsigned int getCategory() const;
	virtual void Move(int direccion);
	virtual void Stop(int direccion);
	virtual void AnimationControl(AnimationControlEnum::ID animationControlEnum);


private:
	
	virtual void drawCurrent(sf::RenderTarget& target, sf::RenderStates states) const;

	Type mType;
	sf::Sprite mSprite;
	float	mPlayerBaseSpeed;
	float	mPlayerIncreasedSpeed;
	sf::IntRect mIntRect;
};
