#include "World.h"
#include <SFML/Graphics/RenderWindow.hpp>

extern float gScrollSpeed;
extern float gIncreasePlayerSpeed;
World::World(sf::RenderWindow& window)
	: mWindow(window)
	, mWorldView(window.getDefaultView())
	, mTextures()
	, mSceneGraph()
	, mSceneLayers()
	, mWorldBounds(0.f, 0.f, mWorldView.getSize().x, 2000.f)
	, mSpawnPosition(mWorldView.getSize().x / 2.f, mWorldBounds.height - mWorldView.getSize().y / 2.f)
	, mScrollSpeed(gScrollSpeed)
	,mPlayerIncreasedSpeed(gIncreasePlayerSpeed)
	, mPlayer(nullptr)
	,mCommandQueue()
	
{
	loadTextures();
	buildScene();

	// Prepare the view
	mWorldView.setCenter(mSpawnPosition);
	//mCommandList.empty();


}

void World::update(sf::Time dt)
{
	// Scroll the world
	mWorldView.move(0.f, mScrollSpeed * dt.asSeconds());

	// Move the player sidewards (plane scouts follow the main aircraft)
	sf::Vector2f position = mPlayer->getPosition();
	sf::Vector2f velocity = mPlayer->getVelocity();

	auto listOfCommands = mCommandQueue.GetCommandList();
	std::list<std::shared_ptr<Command>>::iterator it3;
	for (it3 = listOfCommands.begin(); it3 != listOfCommands.end(); ++it3) {
		auto cmd = it3->get();
		cmd->Execute(*mPlayer);
	}
	if (listOfCommands.size() > 30)
		mCommandQueue.pop();

	// Apply movements
	mSceneGraph.update(dt);
}

void World::draw()
{
	mWindow.setView(mWorldView);
	mWindow.draw(mSceneGraph);
}

template<typename CommandX>
void World::InsertInCommandQueue(std::shared_ptr<CommandX> cmd)
{

	mCommandQueue.push(cmd);

}

void World::InsertInCommandQueueStandar(std::shared_ptr<Command> cmd)
{
	mCommandQueue.pushCommand(cmd);

}


void World::loadTextures()
{
	//std::cout << std::filesystem::current_path() << endl;
	mTextures.load(Textures::ID::PlayerType1, "..\\Asstes\\Images\\player.png");
	//mTextures.load(Textures::PlayerType2, "..\\Asstes\\Images\\boat.png");
	mTextures.load(Textures::ID::PlayerType2, "..\\Asstes\\Images\\BackWater2.png");
	mTextures.load(Textures::ID::Water, "..\\Asstes\\Images\\BackWater2.png");

}

void World::buildScene()
{
	// Initialize the different layers
	for (std::size_t i = 0; i < Layers::Layer::LayerCount; ++i)
	{
		std::unique_ptr<SceneNode> layer(new SceneNode());
		mSceneLayers[i] = layer.get();

		mSceneGraph.attachChild(std::move(layer));
	}

	// Prepare the tiled background
	sf::Texture& texture = mTextures.get(Textures::ID::Water);
	sf::Vector2f texturScale(0.5f, 0.5f);
	auto corrctionMultiplier = 2;
	sf::FloatRect factorScaleCorrection(mWorldBounds.top, mWorldBounds.left, mWorldBounds.width* corrctionMultiplier, mWorldBounds.height* corrctionMultiplier);
	sf::IntRect textureRect(factorScaleCorrection);
	//sf::IntRect textureRect(0,0,5000,5000);
	texture.setRepeated(true);
	
	// Add the background sprite to the scene
	std::unique_ptr<SpriteNode> backgroundSprite(new SpriteNode(texture, textureRect, texturScale));
	backgroundSprite->setPosition(mWorldBounds.left, mWorldBounds.top);
	
	//backgroundSprite->setPosition(mWorldBounds.left, mWorldBounds.top);
	mSceneLayers[Layers::Layer::Background]->attachChild(std::move(backgroundSprite));

	
	// Add player's aircraft
	std::unique_ptr<Player> leader(new Player(Player::PlayerType1, mTextures, mPlayerIncreasedSpeed));
	mPlayer = leader.get();
	mPlayer->setPosition(mSpawnPosition);
	mPlayer->setVelocity(0.f, mScrollSpeed);
	mSceneLayers[Layers::Layer::Water]->attachChild(std::move(leader));

	// Add two escorting aircrafts, placed relatively to the main plane
	//std::unique_ptr<Player> leftEscort(new Player(Player::PlayerType2, mTextures));
	//leftEscort->setPosition(-80.f, 50.f);
	//mPlayer->attachChild(std::move(leftEscort));
	//
	//std::unique_ptr<Player> rightEscort(new Player(Player::PlayerType2, mTextures));
	//rightEscort->setPosition(80.f, 50.f);
	//mPlayer->attachChild(std::move(rightEscort));

	//std::unique_ptr<CommandQueue> cmdQueue(new CommandQueue());
	//mCommandQueue = cmdQueue.get();

}