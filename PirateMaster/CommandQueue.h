#pragma once
#include "Command.h"
#include "MovementCommand.h"
#include "StopperCommand.h"
#include "CommandTypeEnum.h"
#include <array>
#include <queue>
#include <list>


class CommandQueue
{
public:
	CommandQueue();

	template<typename CommandType> 
	void	push( std::shared_ptr<CommandType> command);
	
	//void	pushMovemnt( std::shared_ptr<MovementCommand> command);
	//void	pushStoppers( std::shared_ptr<StopperCommand> command);
	void	pushCommand( std::shared_ptr<Command> command);
    std::list<std::shared_ptr<Command>>& GetCommandList();
	void  pop();
	bool isEmpty();
	void Clear();


private:
	//std::list<std::shared_ptr<MovementCommand>>	mQueueMove;
	//std::list<std::shared_ptr<StopperCommand>>	mQueueStopper;
	std::list<std::shared_ptr<Command>>	mQueueCommand;
	
};