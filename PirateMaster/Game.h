#pragma once
#include <SFML/Graphics.hpp> 
#include <SFML/Audio.hpp>
#include "TexturesIdEnum.h"
#include "ResourceHolder.h"
#include "World.h"
#include "InputHandler.h"
#include "MovementCommand.h"
#include "StopperCommand.h"
#include "Menu.h"
#include "StateStack.h"


class Game
{
public:
	Game();
	void run();
private:
	void processEvents();
	void update(sf::Time deltaTime);
	void render();
	void updateStatistics(sf::Time elapsedTime);
	void handlePlayerInput(sf::Keyboard::Key key, bool isPressed);
private:
	sf::RenderWindow mWindow;
	World	mWorld;
	sf::Text	mStatisticsText;
	sf::Time	mStatisticsUpdateTime;
	sf::Font     mFont;
	std::size_t	mStatisticsNumFrames;
	sf::Sprite mPlayer;
	sf::Texture mTexture;
	sf::Music theme;
	InputHandler mInputHandler;
	std::list<std::shared_ptr<MovementCommand>> mCommandList; 
	Menu menu;
	StateStack mStateStack;
	

};

