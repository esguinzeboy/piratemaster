#pragma once
#include <SFML/Graphics.hpp> 
#include "Player.h"
#include "Command.h"
#include "DirectionEnum.h"

class MovementCommand : public Command
{
public:
	virtual ~MovementCommand() {}
	MovementCommand(DirectionEnum::Direction dir );
	virtual void Execute(Entity& entity);
	virtual void Undo(Entity& entity);
private:
	DirectionEnum::Direction direction;
	



};
