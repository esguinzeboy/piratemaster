#pragma once
#include "Entity.h"
#include "TexturesIdEnum.h"
#include "ResourceHolder.h"
#include <SFML/Graphics.hpp>
class Enemy : public Entity
{
public:
	enum Type
	{
		EnemyType1,
		EnemyType2,
		EnemyType3,
	};
public:
	explicit Enemy(Type type, ResourceHolder<sf::Texture, Textures::ID>& textureHolder);
	virtual unsigned int getCategory() const;
	virtual void Move(int direccion);
	virtual void Stop(int direccion);

private:

	virtual void drawCurrent(sf::RenderTarget& target, sf::RenderStates states) const;

	Type mType;
	sf::Sprite mSprite;
	float	mEnemyBaseSpeed;
	float	mEnemyIncreasedSpeed;
};