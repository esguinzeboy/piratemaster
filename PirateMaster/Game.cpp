#include "Game.h"

#include "iostream"
#include "TextureHolder.h"
#include "ResourceHolder.h"
#include <sstream>
#include "stringHelper.h"
#include "FontsIdEnum.h"
#include "Menu.h"
#include "StateIdEnum.h"


bool mIsPaused;
sf::Vector2f centerPosition;
float PlayerSpeed = 200.f;
sf::Time TimePerFrame = sf::seconds(1.f / 60.f);
ResourceHolder <sf::Texture, Textures::ID> resourceHolderTexture;
ResourceHolder <sf::Font, Fonts::ID> resourceHolderFonts;
sf::Text pausePop;



Game::Game()
	: mWindow(sf::VideoMode(640, 480), "World", sf::Style::Close)
	, mWorld(mWindow)
	, mFont()
	, mStatisticsText()
	, mStatisticsUpdateTime()
	, mStatisticsNumFrames(0)
	, mInputHandler()
	, mCommandList()
	, menu(640, 480, mFont)
	, mStateStack(States::State::Menu)
{
	mFont.loadFromFile("..\\Asstes\\Fonts\\Minecraftia-Regular.ttf");
	mStatisticsText.setFont(mFont);
	mStatisticsText.setPosition(5.f, 5.f);
	mStatisticsText.setCharacterSize(10);
	centerPosition.x = 50.f;
	centerPosition.y = mWindow.getDefaultView().getSize().y / 2.f;
	pausePop.setFont(mFont);
	pausePop.setCharacterSize(70);
	pausePop.setString("Game Paused");
	pausePop.setPosition(centerPosition);
	menu.LoadFont(mFont);
	for (std::size_t i = 0; i < MenuItems::ID::Last; ++i)
	{

		std::shared_ptr<MenuItem> mi(new MenuItem(240, 100 * (i + 1), mFont, static_cast<std::int16_t>(i)));
		menu.SetPositionOfItem(mi);
	}
	menu.SetSelectedItemIndex(0);


	//resourceHolderTexture.load(Textures::Airplane, "C:\\Users\\jferr\\source\\repos\\PirateMaster\\Asstes\\Images\\boat.png");
	//sf::Sprite playerPlane;
	//
	//mPlayer.setTexture(resourceHolderTexture.get(Textures::Airplane));
	//mPlayer.setPosition(100.f, 100.f);
	//theme.openFromFile("C:\\Users\\jferr\\source\\repos\\PirateMaster\\Asstes\\Sounds\\Music\\Apoxode_-_Whale_Watcher.wav");
}

void Game::run()
{
	sf::Clock clock;
	sf::Time timeSinceLastUpdate = sf::Time::Zero;
	while (mWindow.isOpen())
	{
		sf::Time elapsedTime = clock.restart();
		timeSinceLastUpdate += elapsedTime;
		while (timeSinceLastUpdate > TimePerFrame)
		{
			timeSinceLastUpdate -= TimePerFrame;

			processEvents();
			if (!mIsPaused)
				update(TimePerFrame);
		}

		updateStatistics(elapsedTime);
		render();
	}
}


void Game::processEvents()
{
	sf::Event event;
	while (mWindow.pollEvent(event))
	{
		if (event.type == sf::Event::GainedFocus)
			mIsPaused = false;
		else if (event.type == sf::Event::LostFocus)
			mIsPaused = true;

		switch (event.type)
		{
		case sf::Event::KeyPressed:
			handlePlayerInput(event.key.code, true);
			break;

		case sf::Event::KeyReleased:
			handlePlayerInput(event.key.code, false);
			break;
		case sf::Event::Closed:
			mWindow.close();
			break;
		}
	}
}

void Game::update(sf::Time deltaTime)
{

	mWorld.update(deltaTime);
	menu.update(deltaTime);


}

void Game::render()
{

	mWindow.clear();

	switch (mStateStack.GetCurrentState())
	{
	case States::State::Menu:
		mWindow.draw(menu);
		break;
	case States::State::Game:
		mWorld.draw();
		mWindow.setView(mWindow.getDefaultView());
		if (mIsPaused)
		{
			mWindow.draw(pausePop);
		}
		break;
	case States::State::GameOver:
		break;
	case States::State::Win:
		break;
	case States::State::Loading:
		break;
	default:
		break;
	}


	//mWorld.draw();
	//mWindow.setView(mWindow.getDefaultView());
	//if (mIsPaused)
	//{
	//	mWindow.draw(pausePop);
	//	mWindow.draw(menu);
	//}
	mWindow.draw(mStatisticsText);
	mWindow.display();
}

void Game::updateStatistics(sf::Time elapsedTime)
{
	mStatisticsUpdateTime += elapsedTime;
	mStatisticsNumFrames += 1;

	if (mStatisticsUpdateTime >= sf::seconds(1.0f))
	{
		mStatisticsText.setString(
			"Frames / Second = " + toString(mStatisticsNumFrames) + "\n" +
			"Time / Update = " + toString(mStatisticsUpdateTime.asMicroseconds() / mStatisticsNumFrames) + "us");

		mStatisticsUpdateTime -= sf::seconds(1.0f);
		mStatisticsNumFrames = 0;
	}
}


void Game::handlePlayerInput(sf::Keyboard::Key key, bool isPressed)
{
	auto command = mInputHandler.handlePlayerInputPress(key);
	std::shared_ptr<Command> cmd;
	cmd = command;

	if (command && isPressed)
	{
		mWorld.InsertInCommandQueueStandar(cmd);
		menu.InsertInCommandQueueStandar(cmd);
	}

	auto commandStop = mInputHandler.handlePlayerInputRelease(key);
	cmd = commandStop;
	if (commandStop && !isPressed)
		mWorld.InsertInCommandQueueStandar(cmd);

	if (sf::Keyboard::isKeyPressed(sf::Keyboard::Escape))
		mIsPaused = !mIsPaused;


	if (sf::Keyboard::isKeyPressed(sf::Keyboard::L))
		mStateStack.SetCurrentState(States::State::Game);

	if (sf::Keyboard::isKeyPressed(sf::Keyboard::Enter))
	{
		auto currentState = mStateStack.GetCurrentState();

		switch (currentState)
		{
		case States::Loading:
			break;
		case States::Menu:
			if (menu.GetSelectedItemIndex() == 0)
				mStateStack.SetCurrentState(States::State::Game);
			else if (menu.GetSelectedItemIndex() == 3)
				mWindow.close();
			break;
		case States::Game:
			break;
		case States::Win:
			break;
		case States::GameOver:
			break;
		case States::LastOne:
			break;
		default:
			break;
		}
	}


}





