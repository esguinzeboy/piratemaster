#pragma once
namespace CommandTypeEnum
{
    enum class CMDID
    {
        PlayerMovemnt,
        PlayerStop,
        EnemiMovement,
        EnemiStop,
        PowerUpMovement,
        PowerUpStop,
    };
}