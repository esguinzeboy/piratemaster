#pragma once
namespace AnimationControlEnum
{
    enum class ID
    {
        IDLE,
        UP,
        DOWN,
        LEFT,
        RIGHT,
        TAKEDAMAGE,
        DESTROY,
        WINING,
        GAMEOVER,
        LAST,
    };
}