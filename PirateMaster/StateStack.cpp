#include "StateStack.h"

StateStack::StateStack()
{
}

StateStack::StateStack(States::State state): mStateList(),mCurrentState(state)
{
}

void StateStack::PushState(States::State state)
{
	mStateList.push_back(state);
}

void StateStack::PopState(States::State state)
{
	mStateList.remove(state);
}

std::list<States::State> StateStack::GetAllPosibleStates()
{
	return mStateList;
}

void StateStack::SetCurrentState(States::State state)
{
	mCurrentState = state;
}

States::State StateStack::GetCurrentState()
{
	return mCurrentState;
}
