#include "PausedPopUp.h"

PausedPopUp::PausedPopUp(const std::string& string, const sf::Font& font, const int charSize) : mText(string,font)
{
	//mText.setPosition(position);
	mText.setCharacterSize(charSize);
	
}


void PausedPopUp::drawCurrent(sf::RenderTarget& target, sf::RenderStates states) const
{
	target.draw(mText, states);

}