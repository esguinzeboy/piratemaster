#include "SpriteNode.h"

 SpriteNode::SpriteNode(const sf::Texture& texture) : mSprite(texture)
{
}

 SpriteNode::SpriteNode(const sf::Texture& texture, const sf::IntRect& rect) : mSprite(texture, rect) 
 {
 
 }
SpriteNode::SpriteNode(const sf::Texture& texture,	const sf::IntRect& rect, const sf::Color& color) : mSprite(texture, rect)
{
	mSprite.setColor(color);
}

SpriteNode::SpriteNode(const sf::Texture& texture, const sf::IntRect& rect, const sf::Vector2f& scale) : mSprite(texture, rect)
{
	mSprite.setScale(scale);;
}


 void SpriteNode::drawCurrent(sf::RenderTarget& target, sf::RenderStates states) const
{
	 target.draw(mSprite, states);

}