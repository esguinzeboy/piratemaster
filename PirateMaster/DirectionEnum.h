#pragma once
namespace DirectionEnum
{
    enum class Direction
    {
        Up,
        Down,
        Left,
        Right,
    };
}