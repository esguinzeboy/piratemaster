#pragma once
#include <SFML/Graphics.hpp> 
#include "StateStack.h"
#include "StateIdEnum.h"
class State
{
public:
	State(StateStack& stack);
	virtual ~State();
	virtual void draw() = 0;
	virtual bool update(sf::Time dt) = 0;
	virtual bool handleEvent(const sf::Event& event) = 0;
protected:
	void requestStackPush(States::State stateID);
	void requestStackPop();
	void requestStateClear();
	//Context getContext() const;
private:
	StateStack* mStack;
	//Context mContext;
};
