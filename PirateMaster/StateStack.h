#pragma once
#include <list>
#include "StateIdEnum.h"

class StateStack 
{
public:
	StateStack();
	StateStack(States::State state);
	void PushState(States::State state);
	void PopState(States::State state);
	std::list<States::State> GetAllPosibleStates();
	void SetCurrentState(States::State state);
	States::State GetCurrentState();

	
private:
	std::list<States::State> mStateList;
	States::State  mCurrentState;
};
