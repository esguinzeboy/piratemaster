#include "EnemySpawner.h"

EnemySpawner::EnemySpawner():mEnemyList()
{
}

void EnemySpawner::pushEnemy(std::shared_ptr<Enemy> enemy)
{
	mEnemyList.push_back(enemy);
}

std::list<std::shared_ptr<Enemy>>& EnemySpawner::GetEnemies()
{
	return mEnemyList;
}

void EnemySpawner::pop(bool isPopBack)
{
	if (isPopBack)
		mEnemyList.pop_back();
	else
		mEnemyList.pop_front();
}

bool EnemySpawner::isEmpty()
{
	return mEnemyList.empty();
}

void EnemySpawner::Clear()
{
	mEnemyList.clear();
}

void EnemySpawner::destoryEnemy(std::shared_ptr<Enemy>& enemy)
{
	mEnemyList.remove(enemy);
}

