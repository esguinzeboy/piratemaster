#include "InputHandler.h"
#include "StopperCommand.h"



InputHandler::InputHandler():mQueueHolder()
{
}

InputHandler::InputHandler(CommandQueue* mQueueHolder):mQueueHolder(mQueueHolder)
{}

std::shared_ptr<MovementCommand> InputHandler::handlePlayerInputPress(sf::Keyboard::Key key)
{

		if (key == sf::Keyboard::W)
		{
			std::shared_ptr<MovementCommand> cmd(new MovementCommand(DirectionEnum::Direction::Up));
			//list.push_back(cmd);
			return cmd;
		}
		else if (key == sf::Keyboard::S)
		{
			std::shared_ptr<MovementCommand> cmd(new MovementCommand(DirectionEnum::Direction::Down));
		//	list.push_back(cmd);
			return cmd;
		}

		else if (key == sf::Keyboard::A)
		{
			std::shared_ptr<MovementCommand> cmd(new MovementCommand(DirectionEnum::Direction::Left));
			//list.push_back(cmd);
			return cmd;
		}

		else if (key == sf::Keyboard::D)
		{
			std::shared_ptr<MovementCommand> cmd(new MovementCommand(DirectionEnum::Direction::Right));
		//	list.push_back(cmd);
			return cmd;
		}

		else
			return NULL;
	
	
}

std::shared_ptr<StopperCommand> InputHandler::handlePlayerInputRelease(sf::Keyboard::Key key)
{
	
	if (key == sf::Keyboard::W)
	{
		std::shared_ptr<StopperCommand> cmd(new StopperCommand(DirectionEnum::Direction::Up));
		return cmd;
	}
	else if (key == sf::Keyboard::S)
	{
		std::shared_ptr<StopperCommand> cmd(new StopperCommand(DirectionEnum::Direction::Down));
		return cmd;
	}

	else if (key == sf::Keyboard::A)
	{
		std::shared_ptr<StopperCommand> cmd(new StopperCommand(DirectionEnum::Direction::Left));
		return cmd;
	}

	else if (key == sf::Keyboard::D)
	{
		std::shared_ptr<StopperCommand> cmd(new StopperCommand(DirectionEnum::Direction::Right));
		return cmd;
	}
	else
	return NULL;
	
}

