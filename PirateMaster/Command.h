#pragma once
#include"Player.h"
#include "CommandTypeEnum.h"

class Command 
{
public:
	virtual ~Command();
	virtual void Execute(Entity& entity)=0;
	 CommandTypeEnum::CMDID typeOfCommand;

};

