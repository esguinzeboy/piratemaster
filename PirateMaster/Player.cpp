#include "Player.h"
#include "GlobalVariables.h"
#include "TexturesIdEnum.h"
#include "Category.h"
extern float gScrollSpeed;

Textures::ID toTextureID(Player::Type type)
{
	switch (type)
	{
	case Player::PlayerType1:
		return Textures::ID::PlayerType1;
	case Player::PlayerType2:
		return Textures::ID::PlayerType2;
	}
	return Textures::ID::PlayerType1;
}
Player::Player(Type type, ResourceHolder<sf::Texture, Textures::ID>& textureHolder, float increasePlayerSpeed):mType(type),mSprite(textureHolder.get(toTextureID(type))), mPlayerBaseSpeed(gScrollSpeed),mPlayerIncreasedSpeed(increasePlayerSpeed),mIntRect()
{
	sf::FloatRect bounds = mSprite.getLocalBounds();
	mSprite.setOrigin(bounds.width / 2.f, bounds.height / 2.f);
	sf::IntRect rectSourceSprite(142, 0, 48, 48);
	mIntRect = rectSourceSprite;
	mSprite.setTextureRect(mIntRect);

}

unsigned int Player::getCategory() const
{

	switch (mType)
	{
	case PlayerType1:
		return Category::Player;

	default:
		return Category::Enemy;
	}
}

void Player::Move(int direccion)
{
	sf::Vector2f velocity = getVelocity();
	velocity.x = (direccion == 2 || direccion == 3) ? 0 : velocity.x;
	velocity.y = (direccion == 0 || direccion == 1) ? mPlayerBaseSpeed : velocity.y;

	switch (direccion)
	{
	case 2:
		velocity.x = -mPlayerIncreasedSpeed;
		setVelocity(velocity);
		break;
	case 3:
		velocity.x = mPlayerIncreasedSpeed;
		setVelocity(velocity);
		break;
	case 0:
		velocity.y = -mPlayerIncreasedSpeed + mPlayerBaseSpeed;
		setVelocity(velocity);
		break;
	case 1:
		velocity.y = mPlayerIncreasedSpeed + mPlayerBaseSpeed;
		setVelocity(velocity);
		break;
	default:
		break;

	}
		AnimationControl(AnimationControlEnum::ID::IDLE);
}

void Player::Stop(int direccion)
{
	sf::Vector2f velocity = getVelocity();

	switch (direccion)
	{
	case 2:
		if (velocity.x < 0) {
		velocity.x = -0;
		setVelocity(velocity);
			}
		break;
	case 3:
		if (velocity.x > 0) {
			velocity.x = 0;
			setVelocity(velocity);
		}
		break;
	case 0:
		if (velocity.y < mPlayerBaseSpeed) {
			velocity.y = mPlayerBaseSpeed;
			setVelocity(velocity);
		}
		break;
	case 1:
		if (velocity.y > mPlayerBaseSpeed) {
			velocity.y = mPlayerBaseSpeed;
			setVelocity(velocity);
		}
		break;
	default:
		break;
	}
}



void Player::drawCurrent(sf::RenderTarget& target, sf::RenderStates states) const
{
	target.draw(mSprite, states);
}


void Player::AnimationControl(AnimationControlEnum::ID animationControlEnum)
{
	switch (animationControlEnum)
	{
	case AnimationControlEnum::ID::IDLE:
		if (mIntRect.left == 142)
		{
			mIntRect.left = 0;
			mSprite.setTextureRect(mIntRect);
		}
		else
		{
			mIntRect.left = 142;
			mSprite.setTextureRect(mIntRect);
		}

		break;
	case AnimationControlEnum::ID::UP:
		break;
	case AnimationControlEnum::ID::DOWN:
		break;
	case AnimationControlEnum::ID::LEFT:
		break;
	case AnimationControlEnum::ID::RIGHT:
		break;
	case AnimationControlEnum::ID::TAKEDAMAGE:
		break;
	case AnimationControlEnum::ID::DESTROY:
		break;
	case AnimationControlEnum::ID::WINING:
		break;
	case AnimationControlEnum::ID::GAMEOVER:
		break;
	case AnimationControlEnum::ID::LAST:
		break;
	default:
		break;
	}

}


