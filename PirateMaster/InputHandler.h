#pragma once
#include "Command.h"
#include "CommandQueue.h"
#include <SFML/Graphics.hpp> 
#include "Player.h"
#include "MovementCommand.h"
#include "StopperCommand.h"
#include <list>
class InputHandler 
{
public:
	InputHandler(CommandQueue* mQueueHolder);
	InputHandler();
	std::shared_ptr<MovementCommand> handlePlayerInputPress(sf::Keyboard::Key key);
	std::shared_ptr<StopperCommand> handlePlayerInputRelease(sf::Keyboard::Key key);
	//MovementComand* handlePlayerInput(sf::Keyboard::Key key);

private:
	
	CommandQueue* mQueueHolder;
};