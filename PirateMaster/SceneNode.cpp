#include "SceneNode.h"

SceneNode::SceneNode() :mChildren() ,mParent()
{
	mParent = nullptr;
}
void SceneNode::attachChild(std::unique_ptr<SceneNode>  child) 
{
	child->mParent = this;
	mChildren.push_back(std::move(child));
}
std::unique_ptr<SceneNode> SceneNode::detachChild(const SceneNode& node)
{
	auto found = std::find_if(mChildren.begin(), mChildren.end(),
		[&](std::unique_ptr<SceneNode>& p) -> bool { return p.get() == &node; });
	assert(found != mChildren.end());
	std::unique_ptr<SceneNode> result = std::move(*found);
	result->mParent = nullptr;
	mChildren.erase(found);
	return result;
}

void SceneNode::update(sf::Time dt)
{
	updateCurrent(dt);
	updateChildren(dt);
}

void SceneNode::draw(sf::RenderTarget& target, sf::RenderStates states) const
{
	states.transform *= getTransform();
	drawCurrent(target, states);

	for (auto itr = mChildren.begin(); itr != mChildren.end(); ++itr)
	{
		(*itr)->draw(target, states);
	}
}

void SceneNode::drawCurrent(sf::RenderTarget& target, sf::RenderStates states) const
{
}


void SceneNode::updateCurrent(sf::Time)
{
}
void SceneNode::updateChildren(sf::Time dt)
{

	for (auto itr = mChildren.begin(); itr != mChildren.end(); ++itr)
	{
		(*itr)->update(dt);
	}

}

sf::Vector2f SceneNode::getWorldPosition() const
{
	return getWorldTransform() * sf::Vector2f();
}

sf::Transform SceneNode::getWorldTransform() const
{
	sf::Transform transform = sf::Transform::Identity;

	for (const SceneNode* node = this; node != nullptr; node = node->mParent)
		transform = node->getTransform() * transform;

	return transform;
}
