#pragma once
#include "iostream"
#include <vector>
#include <assert.h> 
//#include <SFML/Graphics/Drawable.hpp>
#include <SFML/Graphics.hpp> 
class SceneNode: public sf::Transformable, public sf::Drawable, private sf::NonCopyable
{
//public:
//	typedef std::unique_ptr<SceneNode> Ptr ;
public:
	SceneNode();
	void attachChild(std::unique_ptr<SceneNode>  child);
	std::unique_ptr<SceneNode> detachChild(const SceneNode& node);
	void update(sf::Time dt);
	sf::Vector2f			getWorldPosition() const;
	sf::Transform			getWorldTransform() const;
	//void					onCommand(const Command& command, sf::Time dt);
	//virtual unsigned int	getCategory() const;
private:
	virtual void draw(sf::RenderTarget& target,	sf::RenderStates states) const;
	virtual void drawCurrent(sf::RenderTarget& target,sf::RenderStates states) const;
	virtual void updateCurrent(sf::Time dt);
	void updateChildren(sf::Time dt);

	std::vector<std::unique_ptr<SceneNode>> mChildren;
	SceneNode* mParent;


};
