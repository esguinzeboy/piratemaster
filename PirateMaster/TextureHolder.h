#pragma once
#include "iostream"
#include <filesystem>
#include <map>
#include <SFML/Graphics.hpp> 
#include "TexturesIdEnum.h"

class TextureHolder
{
private:
	std::map<Textures::ID,
		std::unique_ptr<sf::Texture>> mTextureMap;
public:
    void load(Textures::ID id, const std::string& filename);
    sf::Texture& get(Textures::ID id);
    const sf::Texture& get(Textures::ID id) const;
};

