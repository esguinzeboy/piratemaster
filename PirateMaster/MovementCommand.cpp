#include "MovementCommand.h"



MovementCommand::MovementCommand(DirectionEnum::Direction dir) :direction(dir)
{
	typeOfCommand = CommandTypeEnum::CMDID::PlayerMovemnt;
}

void MovementCommand::Execute(Entity& entity)
{

	switch (direction)
	{
	case DirectionEnum::Direction::Up:
		entity.Move((int)DirectionEnum::Direction::Up);
		break;
	case DirectionEnum::Direction::Down:
		entity.Move((int)DirectionEnum::Direction::Down);
		break;
	case DirectionEnum::Direction::Left:
		entity.Move((int)DirectionEnum::Direction::Left);
		break;
	case DirectionEnum::Direction::Right:
		entity.Move((int)DirectionEnum::Direction::Right);
		break;
	default:
		break;
	}


}


void MovementCommand::Undo(Entity& entity)
{
	switch (direction)
	{
	case DirectionEnum::Direction::Up:
		entity.Stop((int)DirectionEnum::Direction::Up);
		break;
	case DirectionEnum::Direction::Down:
		entity.Stop((int)DirectionEnum::Direction::Down);
		break;
	case DirectionEnum::Direction::Left:
		entity.Stop((int)DirectionEnum::Direction::Left);
		break;
	case DirectionEnum::Direction::Right:
		entity.Stop((int)DirectionEnum::Direction::Right);
		break;
	default:
		break;
	}


}