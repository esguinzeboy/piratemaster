#pragma once
#include <SFML/Graphics.hpp> 
#include "Entity.h"
#include "FontsIdEnum.h"
#include <list>
#include <array>
#include <string>
#include "MenuItemIdEnums.h"
class MenuItem : public Entity
{
public:
	MenuItem(float x, float y, sf::Font& font,  std::int16_t indexToInsert);
	~MenuItem();
	//virtual void Move(int direccion);
	std::string toStringId(MenuItems::ID id);
	sf::Text text;
	void Move(int direccion);
	void SetColor(sf::Color);
	void drawCurrentPublic(sf::RenderTarget& target, sf::RenderStates states) const;
private:
	virtual void drawCurrent(sf::RenderTarget& target, sf::RenderStates states) const;
	std::int16_t  index;
};

