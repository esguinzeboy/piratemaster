#pragma once
namespace States
{
    enum  State
    {
        Loading,
        Menu,
        Game,
        Win,
        GameOver,
        LastOne
    };
}