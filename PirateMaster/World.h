#pragma once

#include "MovementCommand.h"
#include "StopperCommand.h"

#include "ResourceHolder.h"
#include "LayerIdEnum.h"
#include "TexturesIdEnum.h"
#include "SceneNode.h"
#include "SpriteNode.h"
#include "Player.h"

#include <SFML/System/NonCopyable.hpp>
#include <SFML/Graphics/View.hpp>
#include <SFML/Graphics/Texture.hpp>

#include <array>
#include <list>
#include "CommandQueue.h"
#include "Enemy.h"
#include "EnemySpawner.h"

//extern float gScrollSpeed;

// Forward declaration
namespace sf
{
	class RenderWindow;
}

class World : private sf::NonCopyable
{
public:
	explicit World(sf::RenderWindow& window);
	void	 update(sf::Time dt);
	void	 draw();
	//void    pushCommand(std::unique_ptr<MovementComand> cmd);
//	template<typename CommandX>
	//void    InsertInListCommand(std::shared_ptr<MovementCommand> cmd);
	//void    InsertInListCommand2(std::shared_ptr<StopperCommand> cmd);
	
	template<typename CommandX>
	void    InsertInCommandQueue(std::shared_ptr<CommandX> cmd);

	//void InsertInCommandQueueMovement(std::shared_ptr<MovementCommand> cmd);
	//void InsertInCommandQueueStopper(std::shared_ptr<StopperCommand> cmd);
	void InsertInCommandQueueStandar(std::shared_ptr<Command> cmd);
	//void copyList(std::list<std::shared_ptr<MovementCommand>>& list);


private:
	void	loadTextures();
	void	buildScene();

private:
	sf::RenderWindow& mWindow;
	sf::View							mWorldView;
	ResourceHolder<sf::Texture, Textures::ID>	mTextures;

	SceneNode							mSceneGraph;
	std::array<SceneNode*, Layers::Layer::LayerCount>	mSceneLayers;

	sf::FloatRect						mWorldBounds;
	sf::Vector2f						mSpawnPosition;
	float								mScrollSpeed;
	float								mPlayerIncreasedSpeed;
	Player* mPlayer;
	CommandQueue mCommandQueue;
	std::unique_ptr<EnemySpawner> EnemySpawner;
	//std::list<std::shared_ptr<Enemy>> mEnmiesList;
	//std::list<std::shared_ptr<StopperCommand>> mCommandListStopper;
};

