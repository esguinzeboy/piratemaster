#include "CommandQueue.h"
#include "SceneNode.h"
#include "MovementCommand.h"
#include "StopperCommand.h"

CommandQueue::CommandQueue()
{
	//mQueue.empty();
}


template<typename CommandType>
void CommandQueue::push(std::shared_ptr<CommandType> command)
{
//	if (CommandType.typeOfCommand == CommandTypeEnum::CMDID::PlayerMovemnt)
//		mQueueMove.push_back(command);
//	else
//		mQueueStopper.push_back(command);
}

void CommandQueue::pop()
{
	mQueueCommand.pop_front();	
}



void CommandQueue::pushCommand(std::shared_ptr<Command> command)
{
	mQueueCommand.push_back(command);
}

std::list<std::shared_ptr<Command>>& CommandQueue::GetCommandList()
{
	return mQueueCommand;
}

bool CommandQueue::isEmpty()
{
	return mQueueCommand.empty();
}

void CommandQueue::Clear()
{
	mQueueCommand.clear();
}
