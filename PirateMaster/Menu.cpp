#include "Menu.h"

Menu::Menu(float width, float height, sf::Font font) :font(font)
{

	this->setPosition(00.f, 00.f);

	
}

Menu::~Menu()
{

}

void Menu::Move(int direccion)
{

	auto currentIndex = this->selectdItemIndex;
	auto maxIndexs = this->mMenuItems.size()-1;


	switch (direccion)
	{
	case 1:
		if (currentIndex == maxIndexs)
			this->SetSelectedItemIndex(0);
		else
			this->SetSelectedItemIndex(currentIndex +1);
		break;
	case 0:
		if (currentIndex == 0)
			this->SetSelectedItemIndex(maxIndexs);
		else
			this->SetSelectedItemIndex(currentIndex - 1);
		break;
	default:
		break;
	}
	
	this->SetChildColor(this->selectdItemIndex);
	
}

void Menu::SetChildColor(int currentIndex) 
{
	int countVar = 0;
	std::list<std::shared_ptr<MenuItem>>::iterator it3;
	for (it3 = mMenuItems.begin(); it3 != mMenuItems.end(); ++it3) {
		auto cmd = it3->get();
		if (countVar==currentIndex)
			cmd->SetColor(sf::Color::Blue);
		else
			cmd->SetColor(sf::Color::White);
		countVar++;
	}
}


void Menu::Stop(int direccion)
{
}

void Menu::update(sf::Time dt)
{
	auto listOfCommands = mCommandQueue.GetCommandList();
	std::list<std::shared_ptr<Command>>::iterator it3;
	for (it3 = listOfCommands.begin(); it3 != listOfCommands.end(); ++it3) {
		auto cmd = it3->get();
		cmd->Execute(*this);
	}
	mCommandQueue.Clear();

}
void Menu::InsertInCommandQueueStandar(std::shared_ptr<Command> cmd)
{
	mCommandQueue.pushCommand(cmd);

}

void Menu::SetPositionOfItem(std::shared_ptr<MenuItem> mi)
{
	auto listLenght = mMenuItems.size();
	mi->setPosition(0.f, 0.f );
	mMenuItems.push_back(mi);
}

void Menu::LoadFont(sf::Font& fontp)
{
	font = fontp;
}

void Menu::SetSelectedItemIndex(int index)
{
	this->selectdItemIndex = index;
	this->SetChildColor(index);
}

int Menu::GetSelectedItemIndex()
{
	return this->selectdItemIndex;
}

void Menu::AssignCommanQue(CommandQueue& cq)
{
	//mCommandQueue = cq;
}

//void Menu::draw(sf::RenderTarget& target, sf::RenderStates states) const
//{
//
//
//	for (auto itr = mMenuItems.begin(); itr != mMenuItems.end(); ++itr)
//	{
//	//	(*itr)->drawCurrent(target, states);
//	}
//}


void Menu::drawCurrent(sf::RenderTarget& target, sf::RenderStates states) const
{
	//states.transform *= getTransform();
	//drawCurrent(target, states);
	for (auto itr = mMenuItems.begin(); itr != mMenuItems.end(); ++itr)
	{
		auto algo = (*itr);
		(*itr)->drawCurrentPublic(target, states);
	}
}
