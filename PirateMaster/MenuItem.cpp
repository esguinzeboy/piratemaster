#include "MenuItem.h"

MenuItem::MenuItem(float x, float y, sf::Font& font, std::int16_t indexToInsert)
{
	text.setFont(font);
	text.setPosition(x, y);
	text.setString(toStringId(static_cast<MenuItems::ID>(indexToInsert)));
	index = indexToInsert;
}

MenuItem::~MenuItem()
{
}

void MenuItem::drawCurrentPublic(sf::RenderTarget& target, sf::RenderStates states) const
{
	drawCurrent(target, states);
}

void MenuItem::drawCurrent(sf::RenderTarget& target, sf::RenderStates states) const
{
	target.draw(text, states);
}

std::string MenuItem::toStringId(MenuItems::ID id)
{
	switch (id)
	{
	case MenuItems::ID::Play:
		return "Play";
	case MenuItems::ID::HowToPlay:
		return "How To Play";
	case MenuItems::ID::Credits:
		return "Credits";
	case MenuItems::ID::Exit:
		return "Exit";
	case MenuItems::ID::Last:
		return "x_x";
	}



}


void MenuItem::SetColor(sf::Color color) 
{

	this->text.setFillColor(color);

}


void MenuItem::Move(int direccion)
{
	sf::Vector2f velocity = getVelocity();
	auto mPlayerIncreasedSpeed = 100;
	velocity.x = (direccion == 2 || direccion == 3) ? 0 : velocity.x;
	velocity.y = (direccion == 0 || direccion == 1) ? 0 : velocity.y;

	switch (direccion)
	{
	case 2:
		velocity.x = -mPlayerIncreasedSpeed;
		setVelocity(velocity);
		break;
	case 3:
		velocity.x = mPlayerIncreasedSpeed;
		setVelocity(velocity);
		break;
	case 0:
		velocity.y = -mPlayerIncreasedSpeed;
		setVelocity(velocity);
		break;
	case 1:
		velocity.y = mPlayerIncreasedSpeed;
		setVelocity(velocity);
		break;
	default:
		break;
	}


}


