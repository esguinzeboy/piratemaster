#include "Enemy.h"

#include "TexturesIdEnum.h"
#include "Category.h"
Textures::ID toTextureID(Enemy::Type type)
{
	switch (type)
	{
	case Enemy::EnemyType1:
		return Textures::ID::EnemyType1;
	case Enemy::EnemyType2:
		return Textures::ID::EnemyType2;
	case Enemy::EnemyType3:
		return Textures::ID::EnemyType3;
	}
	return Textures::ID::EnemyType1;
}
Enemy::Enemy(Type type, ResourceHolder<sf::Texture, Textures::ID>& textureHolder) :mType(type), mSprite(textureHolder.get(toTextureID(type))), mEnemyBaseSpeed(0.f), mEnemyIncreasedSpeed(100.f)
{
	sf::FloatRect bounds = mSprite.getLocalBounds();
	mSprite.setOrigin(bounds.width / 2.f, bounds.height / 2.f);
}

unsigned int Enemy::getCategory() const
{

	switch (mType)
	{
	case EnemyType1:
		return Category::Enemy;

	default:
		return Category::Enemy;
	}
}

void Enemy::Move(int direccion)
{
	sf::Vector2f velocity = getVelocity();
	velocity.x = (direccion == 2 || direccion == 3) ? 0 : velocity.x;
	velocity.y = (direccion == 0 || direccion == 1) ? 0 : velocity.y;

	switch (direccion)
	{
	case 2:
		velocity.x = -mEnemyIncreasedSpeed;
		setVelocity(velocity);
		break;
	case 3:
		velocity.x = mEnemyIncreasedSpeed;
		setVelocity(velocity);
		break;
	case 0:
		velocity.y = -mEnemyIncreasedSpeed;
		setVelocity(velocity);
		break;
	case 1:
		velocity.y = mEnemyIncreasedSpeed;
		setVelocity(velocity);
		break;
	default:
		break;
	}


}

void Enemy::Stop(int direccion)
{
	sf::Vector2f velocity = getVelocity();

	switch (direccion)
	{
	case 2:
		if (velocity.x < 0) {
			velocity.x = -mEnemyBaseSpeed;
			setVelocity(velocity);
		}
		break;
	case 3:
		if (velocity.x > 0) {
			velocity.x = mEnemyBaseSpeed;
			setVelocity(velocity);
		}
		break;
	case 0:
		if (velocity.y < 0) {
			velocity.y = -mEnemyBaseSpeed;
			setVelocity(velocity);
		}
		break;
	case 1:
		if (velocity.y > 0) {
			velocity.y = mEnemyBaseSpeed;
			setVelocity(velocity);
		}
		break;
	default:
		break;
	}
}



void Enemy::drawCurrent(sf::RenderTarget& target, sf::RenderStates states) const
{
	target.draw(mSprite, states);

}





