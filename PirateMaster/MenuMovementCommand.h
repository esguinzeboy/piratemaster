#pragma once
#include <SFML/Graphics.hpp> 
#include "Menu.h"
#include "Command.h"
#include "DirectionEnum.h"

class MenuMovementCommand : public Command
{
public:
	virtual ~MenuMovementCommand() {}
	MenuMovementCommand(DirectionEnum::Direction dir);
	virtual void Execute(Menu& entity);
	virtual void Undo(Menu& entity);
private:
	DirectionEnum::Direction direction;




};
