#include "MenuMovementCommand.h"
 

MenuMovementCommand::MenuMovementCommand(DirectionEnum::Direction dir) :direction(dir)
{
	typeOfCommand = CommandTypeEnum::CMDID::PlayerMovemnt;
}

void MenuMovementCommand::Execute(Menu& entity)
{

	switch (direction)
	{
	case DirectionEnum::Direction::Up:
		entity.Move((int)DirectionEnum::Direction::Up);
		break;
	case DirectionEnum::Direction::Down:
		entity.Move((int)DirectionEnum::Direction::Down);
		break;
	case DirectionEnum::Direction::Left:
		entity.Move((int)DirectionEnum::Direction::Left);
		break;
	case DirectionEnum::Direction::Right:
		entity.Move((int)DirectionEnum::Direction::Right);
		break;
	default:
		break;
	}


}


void MenuMovementCommand::Undo(Menu& entity)
{
	switch (direction)
	{
	case DirectionEnum::Direction::Up:
		entity.Stop((int)DirectionEnum::Direction::Up);
		break;
	case DirectionEnum::Direction::Down:
		entity.Stop((int)DirectionEnum::Direction::Down);
		break;
	case DirectionEnum::Direction::Left:
		entity.Stop((int)DirectionEnum::Direction::Left);
		break;
	case DirectionEnum::Direction::Right:
		entity.Stop((int)DirectionEnum::Direction::Right);
		break;
	default:
		break;
	}


}