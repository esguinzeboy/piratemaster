#pragma once

#include "Enemy.h"
#include <array>
#include <queue>
#include <list>


class EnemySpawner
{
public:
	EnemySpawner();

	void	pushEnemy(std::shared_ptr<Enemy> enemy);
	std::list<std::shared_ptr<Enemy>>& GetEnemies();
	void  pop(bool isPopBack);
	bool isEmpty();
	void Clear();
	void destoryEnemy(std::shared_ptr<Enemy>& enemy);


private:
	//std::list<std::shared_ptr<MovementCommand>>	mQueueMove;
	//std::list<std::shared_ptr<StopperCommand>>	mQueueStopper;
	std::list<std::shared_ptr<Enemy>>	mEnemyList;

};