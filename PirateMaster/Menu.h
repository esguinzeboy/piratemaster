#pragma once

#include <SFML/Graphics.hpp> 
#include "Entity.h"
#include "MenuItemIdEnums.h"
#include "MenuItem.h"
#include "ResourceHolder.h"
#include "FontsIdEnum.h"
#include "CommandQueue.h"
#include <list>
#include <array>
#include <string>




class Menu : public Entity
{

public:
	enum Type
	{
		Play,
		HowToPlay,
		Credits,
		Exit,
		Last
	};
	Menu(float width, float height, sf::Font font);
	~Menu();
	virtual void Move(int direccion);
	virtual void Stop(int direccion);
	void	 update(sf::Time dt);
	virtual void SetPositionOfItem(std::shared_ptr<MenuItem> mi);
	void LoadFont(sf::Font& fontp);
	void SetSelectedItemIndex(int index);
	int GetSelectedItemIndex();
	void AssignCommanQue(CommandQueue& cq);
	void InsertInCommandQueueStandar(std::shared_ptr<Command> cmd);
	void SetChildColor(int currentIndex);

	

	std::list<std::shared_ptr<MenuItem>>	mMenuItems;

private:
	void drawCurrent(sf::RenderTarget& target, sf::RenderStates states) const;
	//std::string toStringId(MenuItems::ID id);
	int selectdItemIndex;
	//ResourceHolder<sf::Font, Fonts::ID>&  mFontHolder;
	sf::Font font;
	//std::list<std::shared_ptr<MenuItem>>	mMenuItems;
	CommandQueue mCommandQueue;
};

